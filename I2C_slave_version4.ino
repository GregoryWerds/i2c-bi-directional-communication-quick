// Help From: https://forum.arduino.cc/t/bidirectional-i2c-communication-with-the-i2c_anything-library/124334/5#msg955223
// I2C communication between 2 Arduinos: slave sketch
#include <Wire.h>
const byte MY_ADDRESS = 42;
const byte OTHER_ADDRESS = 25;
typedef struct {
  int X;
  int Y;
  int Z;
  int U;
  int M;
  int N;
  int O;
  int P;
} twoInts;

void setup(){
  Wire.begin (MY_ADDRESS);
  Serial.begin (9600);
  Wire.onReceive (receiveEvent);
}  // end of setup

volatile twoInts XY;

volatile boolean haveData = false;

void loop(){
  if (haveData){
    //Serial.print ("Received X = ");
    Serial.print ("X: ");
    Serial.print (XY.X); 
//    Serial.print ("Received Y = ");
	Serial.print ("\tY: ");
	Serial.print (XY.Y); 
	Serial.print ("\tZ: ");
	Serial.print (XY.Z); 
	Serial.print ("\tU: ");
	Serial.print (XY.U);
	Serial.print ("\tM: ");
	Serial.print (XY.M);
	Serial.print ("\tN: ");
	Serial.print (XY.N);
	Serial.print ("\tO: ");
	Serial.print (XY.O);
	Serial.print ("\tP: ");
	Serial.println (XY.P);
	
	haveData = false; 
  }  // end if haveData

  twoInts AB;

  AB.X = analogRead(A0);
  AB.Y = analogRead(A1);
  AB.Z = analogRead(A0);
  AB.U = analogRead(A1);
  AB.M = 1;
  AB.N = 2;
  AB.O = 3;
  AB.P = 4;
  
  Wire.beginTransmission (OTHER_ADDRESS);
  Wire.write ((byte *) &AB, sizeof AB);
  Wire.endTransmission ();
  delay(50);

}  // end of loop

// called by interrupt service routine when incoming data arrives
void receiveEvent (int howMany){
  if (howMany < sizeof XY) return;
  // read into structure
  byte * p = (byte *) &XY;
  for (byte i = 0; i < sizeof XY; i++)
    *p++ = Wire.read ();

  haveData = true;     
}
