// Help Came From: https://forum.arduino.cc/t/bidirectional-i2c-communication-with-the-i2c_anything-library/124334/5#msg955223
// I2C communication between 2 Arduinos: master sketch
#include <Wire.h>

const byte MY_ADDRESS = 25;
const byte SLAVE_ADDRESS = 42;

volatile struct {
	int A;
	int B;
	int C;
	int D;
	int E;
	int F;
	int G;
	int H;
} AB;

volatile boolean haveData = false;

void setup(){
	Serial.begin(9600);
	Wire.begin (MY_ADDRESS);
	Wire.onReceive (receiveEvent);
} // end of setup

void loop(){
if (haveData){
	//Serial.print ("Received A = ");
	Serial.print ("A: ");
	Serial.print (AB.A);
	//    Serial.print ("Received B = ");
	Serial.print ("\tB: ");
	Serial.print (AB.B);
	Serial.print ("\tC: ");
	Serial.print (AB.C);
	Serial.print ("\tD: ");
	Serial.print (AB.D);
	Serial.print ("\tE: ");
	Serial.print (AB.E);
	Serial.print ("\tF: ");
	Serial.print (AB.F);
	Serial.print ("\tG: ");
	Serial.print (AB.G);
	Serial.print ("\tH: ");
	Serial.println (AB.H);
	haveData = false;
} // end if haveData

typedef struct {
	int X;
	int Y;
	int Z;
	int U;
	int M;
	int N;
	int O;
	int P;
}  twoInts;
volatile twoInts XY;


XY.X = analogRead(A0);
XY.Y = analogRead(A0);
XY.Z = 22;
XY.U = 23;
XY.M = 1;
XY.N = 1;
XY.O = 0;
XY.P = 1;

Wire.beginTransmission (SLAVE_ADDRESS);
Wire.write ((byte *) &XY, sizeof XY);
Wire.endTransmission ();
delay(50);
}

void receiveEvent (int howMany){
	if (howMany < sizeof AB) return;
	// read into structure
	byte * p = (byte *) &AB;
	for (byte i = 0; i < sizeof AB; i++)
	*p++ = Wire.read ();
	haveData = true;
}
